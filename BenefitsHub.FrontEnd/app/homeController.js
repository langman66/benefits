﻿(function () {
    'use strict';

    angular
        .module('app').controller('homeController', homeController);

    homeController.$inject = ['$location', 'factoryUser'];

    function homeController($location, factoryUser) {

        /* jshint validthis:true */
        var vm = this;
        vm.title = 'homeController';

        vm.user = factoryUser.getUser();

        vm.registerUser = registerUser;

        activate();

        function registerUser() {
            //todo: clientside validation before submitting data
            console.log('clientside validation before registration');

            factoryUser.register()
                .then(function(data) {

                }, function(error) {

                });
        }

        function activate() {
            

        }


    }
})();
