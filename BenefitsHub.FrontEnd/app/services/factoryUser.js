﻿(function () {
    'use strict';

    angular
        .module('app').factory('factoryUser', factoryUser);

    factoryUser.$inject = ['$q','$http'];

    function factoryUser($q, $http) {

        var rootUrl = 'http://localhost:54084/';

        var userData = {
            username: '',
            password: '',
            confirmPassword: '',
            tenantId: -1
        }

        function getUser() {
            return userData;
        }

        function register() {

            $http.post(rootUrl + 'api/Account/Register', { email: userData.username, password: userData.password, confirmPassword: userData.password })
                .then(function(data) {

                }, function(errorData) {

                });

        }

        function login() {
        

            var httpConfig = {
                method: 'POST',
                url: config.remoteServiceName + '/token',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: 'grant_type=password&username=' + userData.username + '&password=' + userData.password
            };

            $http(httpConfig)
                .success(function(data) {
                    userData.isAuthenticated = true;
                    userData.bearerToken = data.access_token;

                })
                .error(function(data) {

                });
        }

        var service = {
            getUser: getUser,
            register: register,
            login: login
        };

        return service;

    }
})();